<?php
// include db connect class
require_once __DIR__ . '/db_connect.php';

// connecting to db
$db = new DB_CONNECT();

if (!mysql_select_db(DB_DATABASE))
    die("Can't select database");

$sql="SELECT * FROM moduleTable ORDER BY moduleName ASC";

$result = mysql_query($sql);

$response["modules"] = array();

while($row = mysql_fetch_array($result))
  {
	  $module = array('moduleNo'=> $row["moduleNo"], 
	  'moduleName'=> $row["moduleName"], 
	  'credits'=> $row["credits"],
	  'dueDate'=> $row["dueDate"],
	  'location'=> $row["location"],
	  'room'=> $row["room"],
	  'lat'=> $row["lat"],
	  'long'=> $row["long"],
	  );
	   array_push($response["modules"], $module);
	}

echo json_encode($response);
mysql_close($con);
?> 